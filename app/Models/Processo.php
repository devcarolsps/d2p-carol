<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Processo extends Model
{
    use HasFactory;
    protected $table = 'Processos';

    public function processosPorCliente($cod_client){
    
        $result =  Processo::where('ClienteCodigo', $cod_client)
        ->where('Ativo', 1)
        ->orderBy('DataDI', 'desc')
        ->get();
        
        return $result;
    }
   
}
