@extends('layout.default')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main">
                <h3>Processos do Cliente: #3256.</h3>


                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th>Número do Processo</th>
                                <th>Número da DI</th>
                                <th>Data da DI</th>
                                <th>Data do Desembaraço</th>
                                <th>Data da Entrega</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($processos) > 0)
                                @foreach ($processos->all() as $processo)
                                    <tr>
                                        <th>{{ $processo->ProcessoID }}</th>
                                        <td>{{ $processo->NumeroDI }}</td>
                                        <td>{{ date('d/m/Y', strtotime($processo->DataDI)) }}</td>
                                        <td>{{ date('d/m/Y', strtotime($processo->DataDesembaraco)) }}</td>
                                        <td>{{ date('d/m/Y', strtotime($processo->DataEntrega)) }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="99">
                                        <div class="text-center p-5 rounded">
                                            Nenhum Processo encontrado
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </main>
        </div>
    </div>
@endsection
